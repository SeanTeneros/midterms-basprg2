#include <iostream>
#include <string>
#include <vector>
#include "time.h"

using namespace std;

const enum Cards
{
	Emperor
	,Civilian
	,Slave
};
const string emperorCard = "Emperor";
const string civCard = "Civilian";
const string slaveCard = "Slave";

const int Multi = 5;
const int betVal = 100000;

void startRound(vector<int>& rounds)
{
	for (int i = 0; i < 12; i++)
	{
		if (( i < 3  || i > 7 && i <= 9 ))//0,1,2
		{
			rounds.push_back(Emperor);
		}
		else if ((i > 3 && i <= 7 || i > 9 && i <= 12 ))//3,4,5
		{
			rounds.push_back(Slave);
		}
	}
}
bool Empside(int play)
{
	if (play == 0) { return true; }
	else if (play == 2) { return false; }
};

void playRound(vector<int>& rounds,int& ed,int& yen,int& currentR)
{
	int cChoice = 0;
	int bet = 0;
	cout << "Player stats: " << endl
		<< "Your Ear drum: " << ed 
		<< endl << "current money: " << yen << endl;
	cout << "Round: "  << currentR + 1 << endl;
	cout << "Place your bet!: " << endl;
	cin >> bet;
	while (bet > ed)
	{
		cout << "Bet is too big!" << endl;
	}
	int Enemycard =rand()% 5;

	if (Empside((rounds[currentR])))
	{
		if (cChoice == 0)
		{
			cout << "Emperor card is chosen!!" << endl;
		
			if (Enemycard == 0)
			{
				cout << "Your Emperor card has lost to Slave" << endl;
				ed -= bet;
			}
			else 
			{
				cout << "Your Emperor card has won against Civilian" << endl;
				ed -= bet;
				yen += betVal * bet ;
			}
		}
		else if (cChoice >= 1)
		{
			cout << "Civilian card is chosen!!" << endl;
			if (Enemycard == 0)
			{
				cout << "Your Civilian card has win against Slave" << endl;
				ed -= bet;
			}
			else
			{
				cout << "Your Civilian card has Tied against Civilian" << endl;
				playRound(rounds, ed, yen, currentR);
			}
		}
	}
	else
	{
		if (cChoice == 0)
		{
			cout << "Slave card is chosen!!" << endl;

			if (Enemycard == 0)
			{
				cout << "Your Slave card has won against Emperor" << endl;
				ed -= bet;
				yen += betVal * bet * 5;
			}
			else
			{
				cout << "Your Slave card has lost against Civilian" << endl;
				ed -= bet;
			}
		}
		else if (cChoice >= 1)
		{
			cout << "Civilian card is chosen!!" << endl;
			if (Enemycard == 0)
			{
				cout << "Your Civilian card has lost against Emperor card" << endl;
				ed -= bet;
			}
			else
			{
				cout << "Your Civilian card has Tied against Civilian" << endl;
				playRound(rounds, ed, yen, currentR);
			}
		}
	}
}
void endGame(int& rounds, int& yen, int& ed)
{
	if (rounds < 12 && yen >= 20000000)
	{
		cout << "Achievement Unlocked: Best Ending" << endl;
		
	}
	else if (rounds < 12 && yen < 20000000)
	{
		cout << "Achievement Unlocked: Meh Ending" << endl;
	}
	else if (ed < 0 && yen < 20000000)
	{
		cout << "Achievement Unlocked: Bad Ending" << endl;
	}
	
system("PAUSE");
}
int main()
{
	int ed = 30;
	int bet = 0;
	int yen = 0;
	int currentR = 0;
	vector<int> rounds;
	while (currentR < 12)
	{
		playRound(rounds, ed, yen, currentR);
		currentR++;
	}
	
	system("pause");
}